<?php

$folder = "uploads/";

$conn = new mysqli("127.0.0.1", "dinogalleria", "9eRn8vSZAFos2ouaL6WQfmIxHzejordH0mPaN88MV9ecW3tu8YL4oQNgD1ysxKk", "gallery");
if ($conn->connect_error) {
  die("Connection to DB failed");
} 


function getCategories() {
  //Get list of categories
  $categories = $GLOBALS['conn']->query("SELECT * FROM category");
  return mysqli_fetch_all($categories,MYSQLI_ASSOC);
}
function getCategoryThumbnail($categoryid) {
  //Change this over to a join statement in getCategories() and push the thumbnail as part of each category subarray
  $thumbnail = $GLOBALS['conn']->query("SELECT src FROM post where category = $categoryid");
  return mysqli_fetch_all($thumbnail,MYSQLI_ASSOC);
}
function getPosts($categoryid) {
  $getPosts = $GLOBALS['conn']->prepare("SELECT * FROM gallery.post WHERE category = ?");
  $getPosts->bind_param("i", $categoryid);
  $getPosts->execute();
  $resultSet = $getPosts->get_result();
  return $resultSet->fetch_all(MYSQLI_ASSOC);
}

function createCategory($name) {
  if ($GLOBALS['conn']->query("INSERT INTO gallery.category (name) VALUES ('$name')")) {
    return true;
  } else {
    return false;
  }
}

function createPost($categoryid, $src, $comment, $credittext, $creditlink) {
  $creditlink = mysqli_real_escape_string($GLOBALS['conn'],$creditlink);
  $sql = "INSERT INTO post (category, src, comment, credittext, creditlink) VALUES ('$categoryid', '$src', '$comment', '$credittext', '$creditlink')";
  if ($GLOBALS['conn']->query($sql) === TRUE) {
    return true;
  } else {
    return false;
  }
}

function deleteCategory($categoryid) {
  if ($GLOBALS['conn']->query("DELETE FROM post WHERE category = $categoryid") && $GLOBALS['conn']->query("DELETE FROM category WHERE idcategory = $categoryid")) {
    return true;
  } else {
    echo $GLOBALS['conn']->error;
    return false;
  }
}
function deletePost($postid) {
  if ($GLOBALS['conn']->query("DELETE FROM post WHERE idpost = $postid")) {
    return true;
  } else {
    echo $GLOBALS['conn']->error;
    return false;
  }
}